%\jo{TODO: fix overloaded symbols and terms between CP and FO}

We implemented the automatic detection of symmetry-induced neighborhoods in the \CSOP{}-solving \idp system. In this section, we will sketch relevant details of the \idp system, as well as the type of symmetry and neighborhoods it detects. We also illustrate the detection by means of the TSP and CNP examples.

\subsection{\idp as a constraint solving system}
The \idp system is an experiment in constructing a \emph{knowledge base} system. The aim of a knowledge base system is to solve problems in a radically declarative way, where domain knowledge is specified once as a knowledge base, allowing the user to solve multiple domain problems without further modifications to the knowledge base. The specification language of \idp is \fodot -- an extension of typed classical first-order logic with aggregates, arithmetic and inductive definitions.

One of the problems the \idp system is capable of solving is a logical \emph{model optimization} problem (\LMOP{}). A \LMOP{} is the logical equivalent of a \CSOP{}, which we will show after the brief introduction of some logical concepts.

In \fodot, logical formulas are constructed using a \emph{vocabulary} \voc, which contains \emph{type} symbols, \emph{predicate} symbols and \emph{function} symbols. The type symbols specify sets of \emph{domain elements} present in the problem, while the predicate and function symbols specify respectively typed relations and typed functions. A \emph{structure} \struct over a vocabulary \voc is an association of actual sets, relations and functions to the symbols in \voc. More formally, we say a structure \struct \emph{interpretes} the symbols in \voc, while for a symbol $P \in \voc$, $P^\struct$ is called its \emph{interpretation}. Structures can be \emph{partial}, in which case some predicate or function symbols have no interpretation.
Given the symbols in a vocabulary, \emph{formula}'s and \emph{terms} can be constructed using logical connectives, quantifiers and other logical symbols. If a structure \struct ranges over the same vocabulary as a formula $\phi$ or a term $t$, then $\phi^\struct$ and $t^\struct$ are evaluations of $\phi$ and $t$: $\phi^\struct$ is either true or false, while $t^\struct$ maps to some domain element $d$ from $\struct$.\footnote{To be exact, $\phi$ and $t$ should also not contain any unquantified logical variables to have a proper evaluation in $\struct$.}
A \emph{theory} \theory over a vocabulary \voc is a set of formulas over \voc, and a structure \struct over \voc \emph{satisfies} \theory if for all $\phi \in \theory$, $\phi^\struct$ is true. In this case, we say that $\struct$ is a \emph{model} of $\theory$, or $\struct \models \theory$.

A \LMOP{} can be characterized as a quadruple $(\Sigma,\structt,\theory,t)$, where $\Sigma$ is the vocabulary, \structt a partial structure over $\Sigma$, \theory a theory over $\Sigma$ and $t$ a term over $\Sigma$ mapping to a numeric domain such as the natural numbers. A \LMOP{} $(\Sigma,\struct,\theory,t)$ then represents the task of finding
\begin{itemize}
\item a model $\struct \models \theory$,
\item such that \struct has the same interpretation as \structt for all types and interpreted symbols in \structt,
\item and \struct is minimal for $t$.\footnote{Again we assume the objective function is to be minimized.}
\end{itemize}
When relating this to a \CSOP{} $(V,D,C,O)$, \theory represents the constraints $C$, $t$ represents the objective function $O$, the uninterpreted function and predicate symbols in \structt represent the variables $V$, and the domain of values $D$ corresponds to the set of possible relations and functions that can be associated to the uninterpreted symbols, given the interpretation of the types of \voc in \structt. A structure corresponds to an assignment, a model to a satisfying assignment, and a minimal model to an optimal solution.

Instead of diving into the technical details of \fodot, we improve a reader's intuition by the following two examples:
\begin{example}
\label{ex_TSP_idp}
The \CSOP{} specification for TSP given in Example \ref{ex_TSP} can be realized as a \LMOP{} with the following vocabulary:
\begin{itemize}
\item type $City$
\item type $Index \subseteq \mathbb{N}$
\item function symbol $Distance: City\times City \rightarrow \mathbb{N}$
\item function symbol $Next: Index \rightarrow Index$
\item function symbol $Map: Index \rightarrow City$
\end{itemize}
For the TSP, every symbol but $Map$ will be interpreted by the partial structure, so $Map$ will function as our search variable.

The TSP constraints are specified by a theory with one formula:
\begin{align*}
\forall x: \forall y: (Index(x) \land Index(y) \land x\neq y) \Rightarrow Map(x)\neq Map(y).
\end{align*}
Which effectively posts an all-different constraint over the $Map$ function symbol, stating that two different index elements need to be mapped to two different cities. Each model satisfying this theory will thus have a TSP tour as interpretation for $Map$.

Next we need a minimization term:
\begin{align*}
\sum_{z} \{Distance(Map(z),Map(Next(z)))~|~z \in Index\}
\end{align*}
Which denotes the sum of all distances between cities mapped by subsequent indices, and as such denotes the total distance of a tour represented by the $Map$ function symbol.

Finally, the partial structure provides the necessary parameters to solve a TSP instance. For this, it contains
\begin{itemize}
\item a set of indices $\{0,\ldots,n-1\}$ and a set of cities $\{c_1,\ldots,c_n\}$ as interpretation for the types,
\item a function adhering to the signature of the $Distance$ symbol specified in the vocabulary,
\item a function adhering to the signature of the $Next$ symbol specified in the vocabulary, which for sensible TSP instances maps some index $x$ to $(x+1)\mod n$.
\end{itemize}

Given the above vocabulary, theory, minimization term and partial structure, \idp's \LMOP{} routine then searches for an interpretation to $Map$ that satisfies the constraints in the theory, and minimizes the objective function. This solves the TSP \CSOP{} described in Example \ref{ex_TSP}, as the inferred interpretation of $Map$ leads to an optimal TSP tour.
\end{example}

\begin{example}
\label{ex_chromatic_number}
The CNP from Example \ref{ex_chromatic} can be modelled in \fodot using the following vocabulary:
\begin{itemize}
\item type $Node$
\item type $Color$
\item predicate symbol $Edge \subseteq Node \times Node$
\item function symbol $Coloring: Node \rightarrow Color$
\end{itemize}

The constraint that two neighboring nodes must have a different color is stated in the following theory:
\begin{align*}
\forall x: \forall y: Edge(x,y) \Rightarrow Coloring(x)\neq Coloring(y).
\end{align*}
And the objective function simply counts the number of colors used:
\begin{align*}
\#\{z~|~\exists x: Coloring(x)=z\}
\end{align*}

Finally, a partial structure contains an input graph by interpreting $Edge$, and leaves the $Color$ symbol uninterpreted. Solving the \LMOP{} will lead to an interpretation for $Color$, representing a minimal graph coloring.
\end{example}

Algorithmically, \idp solves model optimization by a \emph{ground-and-solve} approach, where the  theory, structure and minimization term are \emph{grounded} to a set of low-level constraints in the \emph{extended conjunctive normal form} (ECNF) language. These ECNF constraints can be solved by \idp's custom-made \emph{lazy clause generation} constraint solver \minisatid \cite{ictai/DeCat13}. This grounding step is analogous to the \emph{flattening} of high-level MiniZinc specifications to FlatZinc constraints, to the point that \minisatid can also solve FlatZinc specifications.\footnote{\minisatid also participated in 2014's and 2015's FlatZinc competition.}

%Variables are specified in a logical \emph{vocabulary}, constraints in a logical \emph{theory}, and instance-specific information such as domains and parameters are specified in a logical \emph{structure}. Given these three, \idp follows a \emph{ground-and-solve} approach, turning the \fodot specification into a low-level \emph{extended conjunctive normal form} (ECNF) set of constraints, that can be solved by \minisatid, \idp's backend \emph{lazy clause generation} constraint solver\cite{ictai/DeCat13}. Converting \fodot specifications to ECNF is comparable to how MiniZinc specifications are \emph{flattened} to FlatZinc, to the point that \minisatid can also solve FlatZinc specifications\footnote{\minisatid even participated in last year's FlatZinc competition.}.

\subsection{Symmetry in \fodot}
Given that we defined a symmetry as a permutation of the assignment space preserving satisfaction to the constraints, a symmetry of a \LMOP{} $(\Sigma,\structt,\theory,t)$ corresponds to a permutation of the set of extensions of \structt such that for each extension $\struct$ of $\structt$ holds $\struct \models \theory \Leftrightarrow S(\struct) \models \theory$.

The symmetry type detected by \idp is \emph{domain element swap} (\idpsym{}) symmetry, which is a variant of the type of symmetry detected by the relational model finder Kodkod \cite{tacas/TorlakJ07}. Kodkod uses untyped first-order logic, and hence provides only one set of domain elements $Dom$ in its partial structure \structt. Kodkod's symmetry detection routine then partitions $Dom$ into subsets $Dom_i$ such that for any $Dom_i$ all swaps of domain elements $d_1,d_2 \in Dom_i$ induce a symmetry. \idp's \idpsym{} symmetry similarly exploits the given types as partition of the set of all domain elements, but it does not require all symbols ranging over the type to take part in the symmetry.

Let's provide a formal definition for clarification:

\begin{definition}
\label{def_\idpsym{}}
A \emph{domain element swap} (\idpsym{}) symmetry $S$ of a \LMOP{} $(\Sigma,\structt,\theory,t)$ is a symmetry characterized by a triple $(a,b,\sigma)$ such that $a,b$ are two domain elements from the same type interpretation in $\structt$, and $\sigma$ is a subset of predicate and function symbols from $\Sigma$ that are uninterpreted in \structt. If we take $\pi_{ab}$ to be the permutation of domain elements that swaps $a$ with $b$ and leaves all other domain elements invariant, then $S$ maps each extension \struct of \structt to $S(\struct)$ in such a way that for predicate symbols $P \in \sigma$:
\begin{align*}
(d_1,\dots, d_n) \in P^{\struct} \Leftrightarrow (\pi_{ab}(d_1),\dots, \pi_{ab}(d_n)) \in P^{S(\struct)}
\end{align*}
and for function symbols $f \in \sigma$:
\begin{align*}
d_0=f^{\struct}(d_1,\dots, d_n) \Leftrightarrow \pi_{ab}(d_0)=f^{\struct}(\pi_{ab}(d_1),\dots, \pi_{ab}(d_n))
\end{align*}
while the interpretation of predicate symbols $Q \not \in \sigma$ and function symbols $g \not \in \sigma$ is left untouched:
\begin{align*}
Q^{\struct}=Q^{S(\struct)} \text{ and } g^{\struct}=g^{S(\struct)}
\end{align*}
\end{definition}

%Note that a \idpsym{} symmetry $S$ for a \LMOP{} potentially is a combination of both variable and value symmetry in the \CSOP{} sense: if any output position for some function symbols occurs in $S$'s argument list, it permutes the \CSOP{}'s corresponding values.

The following two examples illustrate \idpsym{} symmetry:

\begin{example}
\label{ex_TSP_sym}
The TSP \LMOP{} from Example \ref{ex_TSP_idp} exhibits two classes of \idpsym{} symmetry:
\begin{itemize}
\item \idpsym{} symmetries characterized by $(i,j,\{Map\})$ for $i,j \in [0 \ldots  n-1]$. These symmetries swap two indices $i$ and $j$ in the interpretation of $Map$, and as such are equivalent to the TSP variable symmetry of Example \ref{ex_sym_in_TSP}.
\item \idpsym{} symmetries characterized by $(c_i,c_j,\{Map\})$ for $i,j \in [1\ldots n]$. These symmetries swap two cities $c_i$ and $c_j$ in the interpretation of $Map$, and as such are equivalent to the TSP value symmetry of Example \ref{ex_sym_in_TSP}.
\end{itemize}
\end{example}

\begin{example}
\label{ex_chromatic_number_sym}
The chromatic number \LMOP{} from Example \ref{ex_chromatic_number} exhibits the following class of \idpsym{} symmetry:
\begin{itemize}
\item \idpsym{} symmetries characterized by $(c,c',\{Coloring\})$ for two domain elements $c, c'$ in $Color$'s interpretation. These symmetries swap two colors $c$ and $c'$ in the interpretation of $Coloring$, and as such are equivalent to the chromatic number value symmetry of Example \ref{ex_chromatic_number}.
\end{itemize}
\end{example}

\subsection{Symmetry-induced neighborhood detection in \idp}
\label{sec_neighb_detection_idp}

As mentioned at the end of Section \ref{cp_ls}, we can convert a \CSOP{} specification to input for a local search algorithm by use of a symmetry-induced neighborhood. The previous section explained the type of symmetry the \idp system can detect for a \LMOP{}%\footnote{The actual \idpsym{} symmetry detection algorithm of \idp is out of the scope of this paper.}
, so all that is left to do is figure out which of these symmetries induce good neighborhoods.

We put forward that symmetries that are invariant for the objective function are bad candidates for neighborhood generation. Often such symmetries constitute a simple renaming of variables, values or domain elements, and as such can not transform a satisfying assignment into a reasonably different one. Of course, sometimes a move in a local search algorithm transforms the current satisfying assignment into one with the same objective value, but having a neighborhood that \emph{only} leads to such moves seems like a waste of resources. We shortly investigate some properties of \idpsym{} symmetry present in a \LMOP{} problem with regard to the invariance of the objective function.

Firstly, a sufficient condition for a \idpsym{} symmetry $(a,b,\sigma)$ to leave the minimization term $t$ %in a \LMOP{} $(\Sigma,\structt,\theory,t)$ 
invariant is that $\sigma$ does not contain any predicate or function symbols occurring in $t$. As such, we can instruct \idp's neighborhood detection algorithm to only investigate symmetries ranging over some uninterpreted symbol in $t$. For the TSP problem specification from Example \ref{ex_TSP_idp} the optimization term contains the uninterpreted symbol $Map$, which occurs in both symbol lists of the TSP \idpsym{} symmetries given in Example \ref{ex_TSP_sym}. As a result, it is possible that both symmetry classes are not invariant for the minimization term, which upon further inspection is the case.

However, ranging over an uninterpreted symbol in the minimization term is not a necessary condition for a \idpsym{} symmetry to be invariant for the minimization term, as shown by the CNP. The CNP \LMOP{} minimization term (see Example \ref{ex_chromatic_number}) contains the function symbol $Coloring$, which also occurs in the list of symbols of the \idpsym{} symmetry given in Example \ref{ex_chromatic_number_sym}. However, swapping two colors in a graph coloring is invariant for the number of colors used. \idp's symmetry detection scheme utilizes more refined mechanisms to detect whether terms and formula's are invariant under a certain \idpsym{} symmetry, which we will use in the experiments, but which will not be explained in further detail here.

With the above points in mind, we can devise a simple symmetry-induced neighborhood detection scheme for a \LMOP{} $(\Sigma,\structt,\theory,t)$ in \idp:
\begin{enumerate}
\item Identify the predicate and function symbols occurring in $t$ but uninterpreted in \structt.
\item Detect \idpsym{} symmetry over these symbols.
\item Ignore any \idpsym{} symmetry invariant for $t$.
\item Convert the remaining \idpsym{} symmetries to neighborhoods by Definition \ref{def_induced_neighborhood}.
\end{enumerate}

The performance critical part of this algorithm is the symmetry detection step, whose efficiency in turn depends on the granularity of the symmetry detected. For \idp, symmetry detection takes at most $O(n^2)$ time, with $n$ the total number of domain elements in \structt, since worst-case it generates all pairs of domain elements $a,b$ to check for \idpsym{} symmetries $(a,b,\sigma)$. The list of symbols $\sigma$ is derivable in linear time from \theory. So for \idpsym{} symmetries, the neighborhood detection mechanism is tractable.

The only question that remains is what (small) set of symmetries should be used to induce neighborhoods, as was mentioned at the end of Section \ref{sec_induce_a_neighborhood}. 
Note that \idpsym{} symmetries represent swaps of domain elements, which can be composed to form other symmetries based on other permutations of domain elements. Moreover, any permutation over a set of domain elements can be obtained by a composition of swaps of domain elements. As a result, detecting a set $\mathcal{S}$ of \idpsym{} symmetries entails detecting a group of symmetries $\Gamma_\mathcal{S}$ that represents the interchangeability of subsets of domain elements (those that can be pairwise swapped).

The size of $\Gamma_\mathcal{S}$ is factorial in the size of $\mathcal{S}$, so using all symmetries in $\Gamma_\mathcal{S}$ as neighborhood inducing symmetries seems an infeasible option. Instead, we restrict the set of neighborhood inducing symmetries to the set of all possible swaps of domain elements, in casu $\mathcal{S}$. This has two advantages: firstly it limits the amount of neighborhood inducing symmetries to $O(n^2)$ with $n$ the number of swappable domain elements. Secondly, $\mathcal{S}$ \emph{generates} $\Gamma_\mathcal{S}$, meaning that any model $S(\struct)$ that can be reached by $S \in \Gamma_\mathcal{S}$ from model $\struct$ can also be reached by a composition of some series of $S' \in \mathcal{S}$.

On the other hand, it is possible to construct a set of symmetries $\mathcal{S}'$ that generates $\Gamma_\mathcal{S}$ but which is $O(n)$ in size. $\mathcal{S}'$ then consists of swaps of subsequent domain elements $d_i,d_{i+1}$ according to some chosen total order on the domain elements. Even though it would lead to smaller neighborhoods, it also skews any local search algorithm according to the chosen order, putting a possibly unwarranted bias on the direction of the search over the solution space. For this reason, we stick with the quadratic set of symmetries $\mathcal{S}$ to induce a neighborhood.
