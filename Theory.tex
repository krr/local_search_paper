\subsection{Constraint Satisfaction Optimization Problems}
A \CSOP{} can be characterized as a quadruple $(V,D,C,O)$ where $V$ denotes a set of variables, $D$ a domain of possible values for these variables, $C$ a set of constraints and $O$ an objective function. An \emph{assignment} to a CSP $\Pi=(V,D,C,O)$ is a function $\alpha: V \rightarrow D$. We refer to the set of all assignments to a \CSOP{} $\Pi$ as its \emph{assignment space} $\assspace_\Pi$.

We abstract a constraint $c \in C$ as the subset of $\assspace_\Pi$ for which $c$ is satisfied. So, $\alpha \in c$ means $\alpha$ satisfies $c$, and $\alpha \not \in c$ means $\alpha$ violates $c$. A \emph{satisfying assignment} to a \CSOP{} is an assignment which satisfies all constraints in $C$. We refer to the set of all satisfying assignments of a \CSOP{} $\Pi$ as the \emph{solution space} $\solspace_\Pi$. Note that the solution space is the intersection of all constraints -- $\solspace_\Pi=\cap_{i} c_i$ with $c_i \in C$ -- and that a \CSOP{} is unsatisfiable if the solution space is empty.

An objective function $O:(V \rightarrow D)\rightarrow \mathbb{N}$ maps assignments to natural numbers. Without loss of generality, we take an \emph{optimal solution} to a \CSOP{} to be a satisfying assignment that is minimal with respect to the objective function. More formally, a satisfying assignment $\alpha \in \solspace_\Pi$ is an optimal solution if $\forall \alpha' \in \solspace_\Pi: O(\alpha) \leq O(\alpha')$.

%\bart{The field of Constraint Programming is concerned with finding solutions of...}

\begin{example}
\label{ex_TSP}
A classical \CSOP{} is the Traveling Salesman Problem (TSP). We can model this problem as a set of cities that need to be visited in a certain order, in such a way that the total distance of the visited tour is minimal. Given $n$ cities, we can use $V=\{v_0,\ldots,v_{n-1}\}$ as a set of variables, the set of cities as domain $D$, and $C$ containing the singular constraint that all variables must be assigned a different city. Given a distance matrix between cities $Dist:D\times D \rightarrow \mathbb{N}$, the objective function $O: \alpha \mapsto \sum_i Dist(\alpha(v_i),\alpha(v_{(i+1)\mod n}))$ maps each assignment to the sum of the distances between two subsequent cities.
\end{example}

\subsection{Local search algorithm}
Local search algorithms use the concept of a \emph{neighborhood} to perform a heuristic walk through the solution space of a \CSOP{}. 
\begin{definition}
\label{def_neighborhoods}
A \emph{neighborhood} $N$ for a \CSOP{} $\Pi$ is a mapping of each satisfying assignment to a set of satisfying assignments $N:\solspace_\Pi \rightarrow \mathcal{P}(\solspace_\Pi)$. $N(\alpha)$ is referred to as the set of \emph{neighbors} of a satisfying assignment $\alpha$ under $N$.
\end{definition}

Local search approaches such as those based on simulated annealing or tabu search require as input a neighborhood $N$ and some initial satisfying assignment $\alpha$. Given these, a typical local search algorithm explores the solution space by enumerating the neighbors of $\alpha$ under $N$. When some neighbor $\alpha' \in N(\alpha)$ satisfies an acceptance criterion (typically based on the objective value of $\alpha'$), it is accepted and becomes the new focus of attention. In a sense, the local search algorithm \emph{moves} from $\alpha$ to $\alpha'$, which is also expressed as executing a \emph{move}.

Search continues by exploring the neighbors of $\alpha'$, until a new neighbor is accepted, leading to a new move, repeating the loop. This loop ends when some stop criterion is met, and the satisfying assignment with the lowest objective value encountered during the search is returned.

The above notion of local search is in a way restrictive, since it does not capture optimization approaches such as evolutionary programming or swarm-based optimization. Nonetheless, metaheuristic methods such as tabu search, simulated annealing, variable neighborhood search or greedy optimization can be characterized by moving from satisfying assignment to satisfying assignment using neighborhoods.

\begin{example}
\label{ex_TSP_neighborhood}
For the TSP problem, a typical neighborhood is the so-called \emph{2-opt} neighborhood. This neighborhood maps each TSP-tour $\alpha$ to a set of new tours by removing a pair of edges, say between cities $c_1,c_2$ and $c_3,c_4$, and reconnecting the resulting two TSP-subpaths by introducing an edge between $c_1,c_4$ and an edge between $c_2,c_3$.
\end{example}

\subsection{Symmetry}
Given the above definition of a neighborhood, it is interesting to investigate its relationship with the notion of symmetry.
We follow \cite{Cohen06symmetrydefinitions} and define a symmetry of a $\CSOP{}$ as a permutation on the assignment space which preserves satisfaction to the constraints:
\begin{definition}
\label{def_symmetry}
A \emph{symmetry} $S$ for a \CSOP{} $\Pi=(V,D,C,O)$ is a permutation on the assignment space $S:(V \rightarrow D) \rightarrow (V \rightarrow D)$ such that $\alpha \in \solspace_\Pi \Leftrightarrow S(\alpha) \in \solspace_\Pi$.
\end{definition}

By Definition \ref{def_symmetry}, every permutation on the assignment space of an unsatisfiable \CSOP{} is a symmetry. This makes Definition \ref{def_symmetry} a very general notion of symmetry, and in practice, only particular types of symmetry are considered. Some examples are \emph{value symmetry}, \emph{variable symmetry}, \emph{row symmetry} and \emph{column symmetry} \cite{compositional_sym_detection}. More often than not, symmetries are induced by permutations on the set of variables or the set of values of a \CSOP:

\begin{definition}
A \emph{variable symmetry} for a \CSOP{} $(V,D,C,O)$ is a symmetry $S_\pi: \alpha \mapsto \alpha \circ \pi$ induced by a permutation $\pi: V \rightarrow V$. A \emph{value symmetry} is a symmetry $S_\rho: \alpha \mapsto \rho \circ \alpha$ induced by a permutation $\rho: D \rightarrow D$.
\end{definition}

Note that Definition \ref{def_symmetry} does not require a symmetry to be invariant with respect to the objective function; a symmetry $S$ is \emph{invariant} for objective function $O$ iff $\forall \alpha: O(\alpha)=O(S(\alpha))$. When symmetry is used to reduce search time by eliminating symmetric parts of the search space through \emph{symmetry breaking}, the broken symmetry $S$ must preserve the implicit minimization constraint of a \CSOP{}. However, in a local search context, we will also investigate symmetries who are \emph{variant} (as in \emph{not invariant}) with respect to the objective function.

\begin{example}
\label{ex_chromatic}
The \emph{Chromatic Number Problem} (CNP) consists of identifying the minimum number of colors with which a graph can be colored such that each two adjacent nodes have a different color. We can model this problem as a \CSOP{} $(V,D,C,O)$ where each node is a variable in $V$, each possible color a value in $D$, the constraints $C$ state that two adjacent nodes in an input graph can not have the same color, and the objective function counts the number of colors used. %An assignment to the CNP represents a coloring of the nodes, regardless of the edges in the graph.

Any permutation $\rho$ of the domain $D$ induces a value symmetry $S_\rho$ for the CNP. Each such $S_\rho$ is invariant for the objective function.
\end{example}

\begin{example}
\label{ex_sym_in_TSP}
Using the TSP model from Example \ref{ex_TSP}, each permutation of the domain induces a value symmetry and each permutation of the variables induces a variable symmetry. Both of these symmetry classes are variant for the objective function.
\end{example}

\subsection{Symmetries induce a neighborhood}
\label{sec_induce_a_neighborhood}
Note that the 2-opt neighborhood of Example \ref{ex_TSP_neighborhood} is based on a particular set of permutations of the set of cities in the TSP-tour. It is striking that these permutations also induce symmetries for the TSP-problem. We formalize this connection between symmetry and neighborhood:

\begin{definition}
\label{def_induced_neighborhood}
Given a set of symmetries $\mathcal{S}$ for some \CSOP{}, the \emph{symmetry-induced neighborhood} $N_\mathcal{S}$ maps each satisfying assignment $\alpha$ to its image under $\mathcal{S}$. More formally, $N_\mathcal{S}: \alpha \mapsto \{S(\alpha)~|~S \in \mathcal{S}\}$.
\end{definition}

By Definition \ref{def_symmetry}, any satisfying assignment has only satisfying assignments as symmetry-induced neighbors, which ensures Definition \ref{def_induced_neighborhood} is a sound neighborhood definition.

Note however that Definition \ref{def_induced_neighborhood} requires some set of symmetries as input. Since a set of symmetries $\mathcal{S}$ forms a group $\Gamma_\mathcal{S}$ under functional composition, the number of possible symmetry sets to form neighborhoods with often is astronomical. In general, we have no definitive answer on what sets of symmetries one should use, but it seems plausible to use some small set $\mathcal{S}$ that generates the detected symmetry group $\Gamma_\mathcal{S}$. This way, each move possible under the induced neighborhood $N_{\Gamma_\mathcal{S}}$ can be simulated by a series of moves under $N_{\mathcal{S}}$, while $N_{\mathcal{S}}$ maps a satisfying assignment to a relatively small set of neighbors.

Using this notion of a symmetry-induced neighborhood, we can automatically compile a \CSOP{} specification to input for a local search algorithm solving the \CSOP{}. Recall that the only input required for many local search algorithms is some initial satisfying assignment $\alpha$ and a neighborhood $N$. The following steps generates these from only the problem specification:
\begin{enumerate}
\item Generate an initial satisfying assignment $\alpha$ using existing constraint programming technoloby.
\item Detect a symmetry group $\Gamma$ of the constraint optimization problem.
\item Use some set of symmetries $\mathcal{S} \subseteq \Gamma$ to construct a symmetry-induced neighborhood $N_\mathcal{S}$.
\end{enumerate}
