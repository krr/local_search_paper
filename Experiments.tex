We implemented the neighborhood detection scheme described in the previous section in \idp, and in this section we experimentally investigate which neighborhoods were detected for a series of well-known constraint optimization problems. The \fodot specifications for each of these problems are available online at \url{adams.cs.
kuleuven.be/idp/localsearch.html}\footnote{Click ``File'', then ''Local Search in \idp''}, where an interested reader can run \idp with one click and see the neighborhood detection mechanism in action.

\subsection{Traveling Salesman Problem: on robustness}
Let us first investigate the TSP problem, since this was our running example. As mentioned in Section \ref{sec_FO_Theory}, both the city-swapping and index-swapping symmetries are variant for the minimization term, resulting in a neighborhood swapping cities and indices.

However, there exist other reasonable \fodot specifications of TSP other than the one in Example \ref{ex_TSP_idp}. For instance, a user could use the following vocabulary, theory and minimization term specifying the TSP:\\

Vocabulary:
\begin{itemize}
\item type $City$
\item function symbol $Distance:City\times City \rightarrow \mathbb{N}$
\item predicate symbol $Following \subseteq City \times City$
\item predicate symbol $Reachable \subseteq City$
\item constant $Start:~\rightarrow City$
\end{itemize}

Theory:
\begin{align*}
&\forall x: \exists1 y: Following(x,y).\\
&\forall y: \exists1 x: Following(x,y).\\
&\forall x: Reachable(x).\\
&\{\forall x: Reachable(x) \leftarrow x=Start \vee (\exists y: Reachable(y) \wedge Following(y,x)).\}
\end{align*}
The constraint between curly brackets is an \emph{inductive definition} \cite{tocl/DeneckerT08}, constraining the $Reachable$ predicate to only be true for cities reachable from a $Start$ city using the $Following$ relation. By next stating that all cities must be reachable, we effectively posted a subtour elimination constraint. \\

Minimization term:
\begin{align*}
\sum_{x,y} \{Distance(x,y)|Following(x,y)\}
\end{align*}

When executing the described neighborhood detection algorithm, we establish that $Following$ is an uninterpreted predicate symbol occurring in the objective function, and that \idpsym{} symmetries swapping cities over this symbol exist. Also, these symmetries are variant for the objective function, so they lead to a city-swapping neighborhood.

This alternative specification experiment shows that the proposed neighborhood detection method exhibits robustness: different specifications of the same problem still lead to comparable neighborhoods. This of course only holds as long as symmetry properties of different specifications are similar.

\subsection{Shortest Path Problem: a succesful problem}
This problem consists of finding the shortest path between a start and end node in a weighted graph. Its specification in \fodot is very similar to the TSP specification of the previous subsection, and centers on finding a minimal interpretation to some $Following/2$ predicate. \idp's neighborhood detection mechanism indeed detected that all two cities except the start and end city were interchangeable. The resulting symmetries were variant for the objective function, leading to a large set of induced neighborhoods. Therefor, we judge \idp's neighborhood detection algorithm to be succesful on the shortest path problem.

\subsection{Max Clique: relaxing constraints?}
The task of a max clique problem is to identify the largest clique in a graph. We modelled this problem in such a way that each satisfying assignment represented a set of nodes forming a clique in the input graph. The only domain elements in this problem are the nodes of the graph, and for typical graphs nodes are not swappable. Hence, \idp could not detect any symmetry, and no induced local search neighborhoods were detected by our algorithm. This makes sense, since there is no obvious way of transforming cliques of a graph in one move to some other clique in the graph. 

However, we can imagine a local search algorithm for this problem that iteratively removes nodes from and to a node set representing a potential clique. This effectively \emph{relaxes} the clique constraint, making any set of nodes a satisfying assignment, regardless of whether it forms a clique in the input graph. The lesson we take from this problem is that our neighborhood detection scheme is not yet able to detect constraints that can be relaxed to allow for a larger potential neighborhood.

\subsection{Chromatic Number Problem: avoiding a useless neighborhood}
As mentioned in Example \ref{ex_chromatic}, the CNP consists of finding the lowest amount of colors with which a graph can be colored so that no two adjacent nodes have the same color. This problem does exhibit symmetry in the sense that all colors to color the nodes with are swappable. However, as mentioned in Section \ref{sec_neighb_detection_idp}, this symmetry is invariant for the objective function, and as a result, no symmetry-induced neighborhood was detected. This is a positive result, since globally swapping colors does not lead to an effective local search neighborhood.

However, swapping colors for each node separately might lead to a reasonable neighborhood, which would again relax the constraint that two adjacent nodes must have a different color. This observation is similar to the one made in the Max Clique section.

\subsection{Knapsack Problem: unexpected symmetry leads to unexpected neighborhood}
The knapsack problem consists of filling up some abstract knapsack with objects, such that the volume of the objects fits the knapsack, but the value of the objects is maximal. Since swapping any two objects in and out of the knapsack might violate the volume constraint, we expected the neighborhood detection algorithm to not detect any symmetry, and thus no neighborhood.

However, \idp's symmetry detection was sufficiently fine-grained to identify that for some instances, some objects had the same volume but a different value. These objects could safely be swapped in and out of the knapsack, leading to an unexpected neighborhood where for a given knapsack, small variations on the filling of the knapsack could be explored.

\subsection{Assignment Problem: human neighborhoods}
The last problem we investigate is the assignment problem, where a bijection between a set of agents and a set of tasks must be found that minimizes the cost of assigning a certain task to a certain agent. As expected, \idp detected that swaps of agents and tasks were not a priori invariant for the objective function. As a result, these symmetries induced a local search neighborhood, which arguably would be the same neighborhood a human algorithmician would devise. This is an optimal neighborhood detection result!

\subsection{Experimental conclusions}
Testing the symmetry-based automated neighborhood detection algorithm of \idp on the above problems yielded interesting insights. Firstly, the approach is robust in changes to the specification as long as the symmetry properties are not disturbed. Secondly, taking invariantness of the minimization term into account allows to avoid detecting useless neighborhoods. Thirdly, for some problems, the symmetry-induced neighborhood is the same as the one a human would devise. Fourthly, to detect more neighborhoods, it might be needed to relax constraints. And finally, sometimes the automated neighborhood detection algorithm might find neighborhoods where a human did not expect them.

On the whole, we evaluate the experiment to have returned positive results, supporting the viability of symmetry-induced neighborhood detection.
